﻿Public Class Form4
    Dim soilType As String = ""
    Dim pr As Decimal
    Dim noDAP As Decimal
    Dim kc As Decimal
    Dim answerLPWR As Decimal
    Dim areaLand As Decimal






    Private Sub ComboBox1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        soilType = ComboBox1.SelectedItem.ToString()
        If soilType = "Loam" Then
            pr = 5

        ElseIf soilType = "Clay" Then
            pr = 1

        ElseIf soilType = "Silt" Then
            pr = 5

        End If
    End Sub



    Private Sub calculateLSR_Click(sender As System.Object, e As System.EventArgs) Handles calculateLSR.Click

        answerLPWR = (intET.Text + pr) / 1000
        txtLPWR.Text = Math.Round(answerLPWR, 2)
        volumeLPWR.Text = Math.Round(answerLPWR * areaLand, 2)
    End Sub

    Private Sub areaLSR_TextChanged(sender As System.Object, e As System.EventArgs) Handles areaLPWR.TextChanged
        areaLand = areaLPWR.Text
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        chooseWindow.Show()
        Me.Hide()
    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        Form1.Show()
        Me.Hide()
    End Sub

    Private Sub Button6_Click(sender As System.Object, e As System.EventArgs) Handles Button6.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        volumeLPWR.Text = ""
        txtLPWR.Text = ""
    End Sub
End Class