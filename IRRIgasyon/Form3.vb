﻿Public Class Form3
    Dim soilType As String = ""
    Dim pr As Decimal
    Dim noDAP As Double
    Dim kc As Decimal
    Dim answerCWR As Decimal
    Dim areaLand As Double = 0






    Private Sub ComboBox1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        soilType = ComboBox1.SelectedItem.ToString()
        If soilType = "Loam" Then
            pr = 5

        ElseIf soilType = "Clay" Then
            pr = 1

        ElseIf soilType = "Silt" Then
            pr = 5

        End If
    End Sub

    Private Sub txtDAP_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtDAP.TextChanged
        noDAP = txtDAP.Text
        If noDAP >= 0 And noDAP <= 20 Then
            kc = 0.95
        ElseIf noDAP > 20 And noDAP <= 40 Then
            kc = 1.05
        ElseIf noDAP > 40 And noDAP <= 70 Then
            kc = 1.1
        ElseIf noDAP > 70 And noDAP <= 90 Then
            kc = 1.1
        ElseIf noDAP > 90 Then
            kc = 0.61
        End If
    End Sub

    Private Sub calculateLSR_Click(sender As System.Object, e As System.EventArgs) Handles calculateLSR.Click

        answerCWR = ((intET.Text * kc) + pr + 50) / 1000
        txtCWR.Text = Math.Round(answerCWR, 2)
        volumeCWR.Text = Math.Round(answerCWR * areaLand, 2)
        'Button1.Focus()
    End Sub

    Private Sub areaLSR_TextChanged(sender As System.Object, e As System.EventArgs) Handles areaCWR.TextChanged
        areaLand = areaCWR.Text
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        volumeCWR.Text = ""
        txtCWR.Text = ""
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        chooseWindow.Show()
        Me.Hide()
    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        Form1.Show()
        Me.Hide()
    End Sub

    Private Sub Button6_Click(sender As System.Object, e As System.EventArgs) Handles Button6.Click
        Me.Close()
    End Sub
End Class